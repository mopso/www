# Public web resources
The source for the public website http://pub.mopso.io

This site uses [hugo](https://gohugo.io) with the [beatifulhugo theme](https://github.com/halogenica/beautifulhugo)
It is hosted in Netlify with netlifycms(see the static/admin directory) and with Netlify identity enabled.

To test this web site locally:
```bash
docker run --rm -it -v $(pwd):/src -p 1313:1313 registry.gitlab.com/pages/hugo:0.92.2 hugo server
```


## Conformance
This repository conforms to [library/doc](https://doc.mopso.io/reference/library/doc) requirements.


## License
© Copyright by [MOPSO](https://mopso.eu/). All right reserved. See LICENSE file.


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
