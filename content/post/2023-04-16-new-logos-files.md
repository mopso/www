---
title: new-official-site
date: 2023-04-16
description: Logo files are now properly cropped
---

The official [logo page](https://pub.mopso.io/legal/official-logo/) contain now cropped logos