---
title: Amlet in Next Generation Internet
date: 2021-12-30
description: Amlet was recognized as a component of the Next Generation Internet
---

Amlet is now part of [Next Generation Internet](https://www.ngi.eu/funded_solution/essi_boc2_45/)