---
title: New doc portal
date: 2022-04-01
description: a new documentation portal released
---
A new version of the documentation portal (https://doc.mopso.io/) was released today.
The new portal is based on google docsy template for hugo system.
It supports dynamic TOC and the printing of entire sections.
