We create the most useful **anti-money laundering solutions for financial institutions** by using the best practices and the leading edge of software technologies.

This site contains public documentation and web resources. For more about us, please refer to the corporate site [mopso.eu](https://mopso.eu)
